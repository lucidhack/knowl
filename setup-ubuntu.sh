#!/bin/bash
##############################################################
## Knowl Bookshelf - System Setup (Bare-Metal Setup v0.1.7) ##
## https://gitlab.com/lucidhack/knowl                       ##
## https://creativecommons.org/licenses/by/4.0/             ##
## Author(s): lucidhack, royalpatch                         ##
##############################################################

PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin

echo "[010] # STARTING THE SETUP SCRIPT"
echo " Starting dts: " date

echo "[020] # INSTALL GENERAL SOFTWARE REQUIREMENTS"
sudo apt-get update -y
sudo apt-get install -y cifs-utils           # For mounting remote ebook shares
sudo apt-get install -y mariadb-server       # To store our databases in
sudo apt-get install -y openjdk-8-jre        # Required for ELK Stack
sudo apt-get install -y unrar                # Extracting archives
sudo apt-get install -y zip                  # Extracting archives
sudo apt-get install -y unzip                # Extracting archives
sudo apt-get install -y p7zip-full           # Extracting archives
sudo apt-get install -y p7zip-rar            # Extracting archives
sudo apt-get install -y apt-transport-https  # Required for Elasticsearch Repo

echo "[030] # INSTALL CORRECT YARN"
sudo apt-get -y remove cmdtest yarn
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get  update -y
sudo apt-get install -y yarn

echo "[050] # ADD ELASTIC.CO REPOSITORY KEY"
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "# SAVE ELASTIC.CO REPOSITORY DEFINITION, AND UPDATE THE REPOSITORY"
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list
sudo apt-get update

echo "[060] # INSTALL ELK STACK (Elasticsearch, Logstash, and Kibana)"
sudo apt-get install -y elasticsearch=7.7.0
sudo apt-get install -y kibana=7.7.0
sudo apt-get install -y logstash

echo "[070] # CONFIGURE ELASTICSEARCH"
# elasticsearch.yml configuration file
sudo rm -f /etc/elasticsearch/elasticsearch.yml.knowl
sudo mv /etc/elasticsearch/elasticsearch.yml /etc/elasticsearch/elasticsearch.yml.knowl
sudo cp ~/knowl/bare-metal/elasticsearch/elasticsearch.yml /etc/elasticsearch
# jvm.options configuration file
sudo rm -f /etc/elasticsearch/jvm.options.knowl
sudo mv /etc/elasticsearch/jvm.options /etc/elasticsearch/jvm.options.knowl
sudo cp ~/knowl/bare-metal/elasticsearch/jvm.options /etc/elasticsearch
sudo systemctl stop elasticsearch
sudo systemctl start elasticsearch

echo "[080] # CONFIGURE KIBANA"
# kibana.yml configuration file
sudo rm -f /etc/kibana/kibana.yml.knowl
sudo mv /etc/kibana/kibana.yml /etc/kibana/kibana.yml.knowl
sudo cp ~/knowl/bare-metal/kibana/kibana.yml /etc/kibana
sudo systemctl stop kibana
sudo systemctl start kibana

echo "[090] # CONFIGURE LOGSTASH"
# jvm.options configuration file
sudo rm -f /etc/logstash/jvm.options.knowl
sudo mv /etc/logstash/jvm.options /etc/logstash/jvm.options.knowl
sudo cp ~/knowl/bare-metal/logstash/jvm.options /etc/logstash
#sudo /bin/systemctl enable logstash.service

echo "[100] # ENABLE ELK STACK ON REBOOT"
sudo /bin/systemctl enable elasticsearch.service
sudo /bin/systemctl enable kibana.service
#sudo /bin/systemctl enable logstash.service

echo "[110] # OPEN FIREWALL PORTS"
sudo ufw allow 9200 # Elasticsearch
sudo ufw allow 5601 # Kibana

echo "[120] # CREATE THE MARIA DATABASE USER: bookuser PASSWORD: password"
sudo mysql <<ENDSQL
    CREATE USER 'bookuser'@'localhost' IDENTIFIED BY 'password';
    FLUSH PRIVILEGES;
ENDSQL

echo "[130] # GRANT bookuser ACCESS TO MariaDB"
sudo mysql <<ENDSQL
    GRANT ALL PRIVILEGES ON * . * TO 'bookuser'@'localhost' IDENTIFIED BY 'password';
    FLUSH PRIVILEGES;
ENDSQL

echo "[140] # INSTALL MARIADB CONNECT PLUGIN"
sudo apt-get install -y libodbc1
sudo apt-get install -y mariadb-plugin-connect
sudo mysql <<ENDSQL
  INSTALL SONAME 'ha_connect';
ENDSQL

echo "[150] # CREATE EMPTY DATA FOLDERS"
sudo mkdir ~/knowl/bookshelf/public/books
sudo mkdir ~/knowl/bookshelf/public/covers

echo "[160] # DOWNLOAD KNOWL BOOKSHELF DEPENDENCIES"
cd ~/knowl/bookshelf
yarn
sudo yarn add searchkit --save
sudo yarn add react-icons-kit --save
sudo yarn add react-gravatar --save
sudo yarn add react-collapsible --save
sudo yarn global add serve
sudo snap install serve
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p

echo "[170] # UPDATE APP.JS WITH OUR TARGET SYSTEMS IP ADDRESS"
targetIP=$(ip route get 8.8.8.8 | sed -n '/src/{s/.*src *\([^ ]*\).*/\1/p;q}')
echo $targetIP
sudo sed -i "s/10.10.10.10:9200/$targetIP:9200/g" ~/knowl/bookshelf/src/App.js

echo "[180] # START KNOWL BOOKSHELF"
echo "Knowl Bookshelf will default to http://localhost:3000"
cd ~/knowl/bookshelf
yarn start
